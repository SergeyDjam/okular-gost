
include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}/..
  ${CMAKE_CURRENT_BINARY_DIR}/../
)

# okular

set(okular_SRCS
   main.cpp
   okular_main.cpp
   shell.cpp
   shellutils.cpp
)

file(GLOB ICONS_SRCS "${CMAKE_CURRENT_SOURCE_DIR}/../icons/*-apps-okular-csp.png")
ecm_add_app_icon(okular_SRCS ICONS ${ICONS_SRCS})

add_executable(okular-csp ${okular_SRCS})

target_link_libraries(okular-csp KF5::I18n KF5::Parts KF5::WindowSystem KF5::Crash)
if(TARGET KF5::Activities)
    target_compile_definitions(okular-csp PUBLIC -DWITH_KACTIVITIES=1)

	target_link_libraries(okular-csp KF5::Activities)
endif()

install(TARGETS okular-csp ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})


# okular shell data files

install( PROGRAMS org.kde.okular-csp.desktop  DESTINATION  ${KDE_INSTALL_APPDIR} )
install( FILES shell.rc DESTINATION ${KDE_INSTALL_KXMLGUI5DIR}/okular-csp )
install( FILES org.kde.okular-csp.appdata.xml DESTINATION  ${KDE_INSTALL_METAINFODIR} )
