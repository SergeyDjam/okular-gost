# Окуляр ГОСТ — программа для просмотра и подписания PDF

Okular GOST can view and annotate documents of various formats, including PDF, Postscript, Comic Book, and various image formats.
It supports native PDF annotations.

### Исходный код

https://gitlab.com/lab50/okular-csp/okular

The Okular GOST repository contains the source code for:
 * the `okular` desktop application (the “shell”),
 * the `okularpart` KParts plugin,
 * the `okularkirigami` mobile application,
 * several `okularGenerator_xyz` plugins, which provide backends for different document types.

### Build instructions

Okular GOST can be built like many other applications developed by KDE.
See https://community.kde.org/Get_Involved/development for an introduction.

If your build environment is set up correctly, you can also build Okular using CMake:

```bash
git clone https://gitlab.com/lab50/okular-csp/okular.git
cd okular
mkdir build
cd build
cmake -DOKULAR_UI=desktop -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir ..
make
make install
```

If you install Okular GOST in a different path than your system install directory it is possible that you need to run

```bash
source build/prefix.sh; okular
```

so that the correct Okular instance and libraries are picked up.

As stated above, Okular has various build targets.
Two of them are executables.
You can choose which executable to build by passing a flag to CMake:

```bash
cmake -DCMAKE_INSTALL_PREFIX=/path/to/your/install/dir -DOKULAR_UI=desktop ..
```
Available options are `desktop`, `mobile`, and `both`.

### clang-format

The Okular GOST project uses clang-format to enforce source code formatting.
See [README.clang_format](https://invent.kde.org/graphics/okular/-/blob/master/README.clang-format) for more information.
